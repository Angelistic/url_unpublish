/**
 * @doc
 * Contains installation and usage information.
 */

Module: Url Unpublish

Description
===========
Url Unpublish: This module is developed for Unpublishing nodes 
through url parameter.

Requirements
============
None.

Installation
============
1. Copy the 'Url Unpublish' module directory in to your Drupal modules' 
directory and install it.

That's it! You are all set to use the module.

Usage
=====
Basically all you have to do the following:

To unpublish a node whose nid is 5 use the following code:

<a href="<?php print base_path().'admin/config/url_unpublish/confirm/5';?>">Unpublish Node</a>
