<?php

/**
 * @file
 * Contains \Drupal\url_unpublish\Form\url_unpublish_confirm.
 */

namespace Drupal\url_unpublish\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\RedirectResponse;

class url_unpublish_confirm extends ConfirmFormBase {

    /**
     * @var string
     */
    protected $nodeId, $nodeTitle;

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'unpublish_node';
    }

    /**
     * {@inheritdoc}
     */
    public function getQuestion() {
        return t('Are you sure you want to unpublish %nodeTitle', array('%nodeTitle' => $this->nodeTitle));
    }

    /**
     * {@inheritdoc}
     */
    public function getCancelUrl() {
        $destinationStore = \Drupal::service('user.private_tempstore')->get('url_unpublish');
        $destination = $destinationStore->get('url_unpublish_destination');
        $reDirectUrl = Url::fromUri(file_create_url($destination));
        return $reDirectUrl;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription() {
        return t('This action cannot be undone.');
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmText() {
        return t('Unpublish');
    }

    /**
     * {@inheritdoc}
     */
    public function getCancelText() {
        return t('Cancel');
    }

    /**
     * {@inheritdoc}
     *
     * @param $node
     *   (optional) The $node which needs to be unpublished.
     */
    public function buildForm(array $form, FormStateInterface $form_state, $node = NULL) {
        if (isset($node)) {
            $nodeInfo = Node::load($node);
            $nodeTitle = $nodeInfo->get('title')->value;
            $nodeId = $nodeInfo->get('nid')->value;
            $this->nodeId = $nodeId;
            $this->nodeTitle = $nodeTitle;
        }
        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $form_values = $form_state->getValues();
        $nid = \Drupal::routeMatch()->getRawParameter('node');
        $node = Node::load($nid);
        if ($form_values['confirm']) {
            $node->status = 0;
            $node->save();
        }
        $url = Url::fromRoute('<front>', [], []);
        $response = new RedirectResponse($url->toString());
        $response->send();
    }
}
