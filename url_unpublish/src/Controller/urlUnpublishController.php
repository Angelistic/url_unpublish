<?php
/**
 * @file
 * Contains \Drupal\url_unpublish\Controller.
 */
namespace Drupal\url_unpublish\Controller;

use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\PrivateTempStoreFactory;

class urlUnpublishController {

    public function url_unpublish_save_destination($node) {
        $previousUrl = \Drupal::request()->server->get('HTTP_REFERER');
        $destinationStore = \Drupal::service('user.private_tempstore')->get('url_unpublish');
        $destinationStore->set('url_unpublish_destination', $previousUrl);

        $form = $this->getConfirmForm();
        return $form;
    }

    function getConfirmForm() {
        $form_obj = new \Drupal\url_unpublish\Form\url_unpublish_confirm();
        $form = \Drupal::formBuilder()->getForm($form_obj);
        $renderArray['form'] = $form;

        $resultForm = \Drupal::formBuilder()->getForm('Drupal\url_unpublish\Form\url_unpublish_confirm');

        $build = [
            'form' => $resultForm,
        ];
        return $build;
    }
}
